<?php

/**
 * @file
 *   Administrative pages for the Tutorials module.
 */

function tutorials_settings() {
  $form = array();

  $form['tutorials_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Tutorials Server'),
    '#default_value' => variable_get('tutorials_server', 'http://tutr.tv'),
    '#description' => t('AJAX callback for getting the list of tutorials. Do not include a trailing slash.'),
    '#required' => TRUE,
  );
  $form['tutorials_default_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Default tutorial selector'),
    '#default_value' => variable_get('tutorials_default_selector', '#content'),
    '#description' => t('Attach links to this jquery selector for any page wide videos.'),
    '#required' => TRUE,
  );
  $targets = array(
    'popup' => 'Popup',
    'new_window' => 'New Window',
  );
  $form['tutorials_target'] = array(
    '#type' => 'select',
    '#title' => t('Location of video popups'),
    '#default_value' => variable_get('tutorials_target', 'popup'),
    '#options' => $targets,
    '#description' => t('Select where video links will open.'),
  );

  return system_settings_form($form);
}
